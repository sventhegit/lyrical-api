# Lyrical API
Dockerised Flask app using the musicbrainzngs package and the liricsovh API

## What it does
This code has been written with the idea that it would go in production. I tried to structure the files in appropriate directories. I added interfaces for clarity and tried to separate the resposibility of the code.

At the moment, there is only one endpoint `/stats/word-average`
which takes the query string `artist_name=`.

Behind the scenes, the muzicbrainzngs library is used to find the artist matching the name. It picks the top item out of the list and gets the 'work-list' for that artist. 

The items in that list contain the title of the song. Using those, we make a call to the LyricsOvh API in order to retrieve the lyrics. For each set of lyrics found. We then count the words and return the average.

### What can be done
- The tests could be made to run as part of the build.
- LyricsOvh integration needs more work


## Build
`docker build -t lyrical-api:1.0.0 .`

## Test
Currently, the tests can be run using `pytest` through using a virtual environment.

First: `python3 -m venv venv && . venv/bin/activate && pip install -r requirements.txt`

Then: `pytest`

## Run
`docker run -d -p 5000:5000 --name lyrical-api lyrical-api:1.0.0`

## Usage

GET call to the link below returns the average number of words the songs by the specified artist has.

`http://localhost:5000/stats/word-average?artist_name=Queens%20of%20the%20stone%20age`

## Destroy
`docker container rm -f lyrical-api && docker image rm lyrical-api:1.0.0`