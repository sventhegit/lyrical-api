
class MusicServiceInterface:
  def __init__(self,**kwargs):
    pass

  def get_word_average_by_artist_name(self, artist: str ):
    pass

  def get_artists_by_name(self, artist: str, options: None):
    pass

  def get_lyrics_for_song(self, artist_name: str, title: str):
    pass