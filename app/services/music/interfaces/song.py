from . import ArtistInterface
class SongInterface:
  id: str
  title: str
  artist: ArtistInterface
  lyrics: str