from .artist import ArtistInterface
from .song import SongInterface
from .music_service import MusicServiceInterface