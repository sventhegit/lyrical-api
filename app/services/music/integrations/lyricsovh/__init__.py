import requests
from app.services.music.interfaces import SongInterface, ArtistInterface
class LyricsOvh:

  def make_request_url(self, artist: str, title: str):
    request_url = 'https://api.lyrics.ovh/v1/%s/%s' % (artist,title)
    return request_url

  def get_lyrics_for_song(self,song: SongInterface, artist: ArtistInterface):
    artist_name = artist.name
    song_title  = song['title']

    request_url = self.make_request_url(artist_name,song_title)

    result = requests.get(request_url)

    if result.status_code == 200:
      return result.json()['lyrics']
    
    return None
    
  
  
