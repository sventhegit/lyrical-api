import musicbrainzngs
from typing import List
from statistics import mean 

from app.services.music.interfaces import MusicServiceInterface
from app.services.music.models import Artist
from app.services.music.integrations.lyricsovh import LyricsOvh

from .utils.serializers import ArtistSerializer
from .utils.string_opertations import count_words_in_string


class MusicBrainzService(MusicServiceInterface):
  def __init__(self,**kwargs):
    self.service = musicbrainzngs
    self.lyrics_provider = LyricsOvh()
    #These must be set in order to be able to make requests
    self.service.set_useragent(app="test-app",version="2",contact=None)

  def get_artists_by_name(self, artist: str, options=None):
    fetched_artists = self.service.search_artists(query=artist)
    deserialized_artists = ArtistSerializer.deserialize_artist_list(fetched_artists)
    return deserialized_artists

  def get_soundtracks_by_artist_id(self,artist_id: str) -> List:
    works = self.service.get_artist_by_id(artist_id, includes=['works'])
    return works['artist']['work-list']

  def get_word_average_by_artist_name(self, artist: str ):
    artists = self.get_artists_by_name(artist=artist)
    if len(artists) < 0:
      return None

    soundtracks = self.get_soundtracks_by_artist_id(artists[0].id)

    counter = []

    for song in soundtracks:
      lyrics = self.lyrics_provider.get_lyrics_for_song(song,artists[0])
      if lyrics is not None:
        count = count_words_in_string(lyrics)
        counter.append(count)

    if len(counter) == 0:
      return "Could not find lyrics for the songs"

    average_word_count = round(sum(counter) / len(counter))

    return f'Average word count is {average_word_count}'