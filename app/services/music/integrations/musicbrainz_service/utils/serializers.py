from app.services.music.models import Artist
from typing import List

class ArtistSerializer():
  @staticmethod
  def deserialize_artist_list(artist_list: dict) -> List[Artist]:
    result = []
    for artist in artist_list['artist-list']:
      result.append(Artist(**artist))
    return result