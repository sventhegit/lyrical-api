import re
def count_words_in_string(string: str):
    return len(re.findall(r'\w+', string))