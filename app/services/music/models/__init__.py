from .utils import get_from_kwargs
from ..interfaces import ArtistInterface, SongInterface

class Artist(ArtistInterface):
  def __init__(self,**kwargs):
    self.id   = get_from_kwargs(kwargs,'id')
    self.type = get_from_kwargs(kwargs,'type')
    self.name = get_from_kwargs(kwargs,'name')

class Song(SongInterface):
  def __init__(self,**kwargs):
    self.id     = get_from_kwargs(kwargs,'id')
    self.title  = get_from_kwargs(kwargs,'type')
    self.artist   = get_from_kwargs(kwargs,'artist',None)