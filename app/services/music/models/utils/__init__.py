
def get_from_kwargs(kwargs,key,default_value=""):
  resolved_value= None
  if key in kwargs and kwargs[key]:
    resolved_value = kwargs[key]
  else:
    resolved_value = default_value
  return resolved_value