import os
from flask import Flask ,g, request, Response
from app.services.music.integrations.musicbrainz_service import MusicBrainzService

def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    # app.config.from_mapping(
    #     SECRET_KEY='dev'
    # )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    music_service = MusicBrainzService()
    
    @app.route('/stats/word-average', methods=['GET'])
    def word_average():
    # pylint: disable=unused-variable
    # pylint: enable=unused-variable
        if request.method == 'GET':
            artist_name = request.args.get('artist_name')
            if not artist_name:
                return Response("Please provide 'artist_name' in url",status=400)
            
            word_average = music_service.get_word_average_by_artist_name(artist=artist_name)

            if not word_average:
              return Response('Could not get average',status=200)
            else:
              return Response(word_average,status=200)
    return app