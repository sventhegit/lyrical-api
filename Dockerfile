FROM python:3.7-alpine

ENV FLASK_APP=app
ENV FLASK_ENV=production
ENV FLASK_PORT=5000
ENV FLASK_HOST=0.0.0.0

COPY . ./

RUN pip3 install -r requirements.txt

CMD flask run --port=$FLASK_PORT --host=$FLASK_HOST

EXPOSE $FLASK_PORT