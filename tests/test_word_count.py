import pytest
import datetime

def test_get_word_average_success(client,app):
  with app.app_context():
    with client:
      response= client.get('/stats/word-average?artist_name=Metallica')

      assert response.status_code == 200